import { Locator, Page } from "playwright";
import ImageCompare from "../utils/ImageCompare";

export default class SpaceLevel1Page {

    private _page: Page;
    private _apiEndPoint: string;

    private btnZoomOut: Locator;
    private txtZoomPercentage: Locator;
    private panelCatalog: Locator;
    private btnDock: Locator;
    private btnRightPanel: Locator;
    private canvaMain: Locator;
    private btnConfirmDelete: Locator;
    private _browserName: string | undefined;

    constructor(page: Page, baseUrl: string, browserName: string | undefined = undefined) {
        this._page = page;
        this._apiEndPoint = baseUrl.replace('app', 'api');
        this._browserName = browserName;
        this.btnZoomOut = this._page.locator(".leftArrow");
        this.txtZoomPercentage = this._page.locator(".zoomPercentage");
        this.panelCatalog = this._page.locator("//div[@title='Catalog Panel']");
        this.btnDock = this._page.locator(".dockBtn .container");
        this.btnRightPanel = this._page.locator(".handler");
        this.canvaMain = this._page.locator("#canvas-div");
        this.btnConfirmDelete = this._page.locator("//div[@data-cy='OkButton']");
    }

    public async ZoomOutSpace(zoomLevel: number = 12) {
        const size = parseInt(await this.txtZoomPercentage.innerText(), 10);

        while (size >= zoomLevel) {
            await this.btnZoomOut.click();
        }
    }

    public async lockCatalogWindow() {
        await this.panelCatalog.click();
        await this.btnDock.click();
    }

    public async showHideRightWindow() {
        await this.btnRightPanel.nth(1).click();
    }

    public async dragDropDevice(device: string, locationX: number | undefined = undefined, locationY: number | undefined = undefined) {
        const sourceDeviceLocation = `//div[@class='typeName' and text()='${device}']/../../..`;
        const destinationLocation = "#canvas-div";
        const canvaBox = await this.canvaMain.boundingBox();
        const responsePromise = this._page.waitForResponse(response =>
            response.url() === `${this._apiEndPoint}/xyicons/create` && response.status() === 200);

        if (canvaBox) {
            const x = locationX == undefined ? (canvaBox.width / 500) : locationX;
            const y = locationY == undefined ? (canvaBox.height / 2) : locationY;

            await this._page.dragAndDrop(sourceDeviceLocation, destinationLocation, { force: true, targetPosition: { x: canvaBox.x + x, y: canvaBox.y + y } });
            if (this._browserName == 'firefox')
                await this.canvaMain.click({ position: { x: canvaBox.x + x, y: canvaBox.y + y } });
            await responsePromise;
        }
    }

    public async selectObejctForDetails(locationX: number | undefined = undefined, locationY: number | undefined = undefined) {
        const canvaBox = await this.canvaMain.boundingBox();

        if (canvaBox) {
            const x = locationX == undefined ? (canvaBox.width / 500) : locationX;
            const y = locationY == undefined ? (canvaBox.height / 2) : locationY;

            await this.canvaMain.click({ position: { x: canvaBox.x + x, y: canvaBox.y + y } });
        }
    }

    public async compareEmbeddedXyicon(iconName: string): Promise<number> {
        const sourceImage = await this._page.locator(`//div[@class='typeName' and text()='${iconName}' ]/../../preceding-sibling::div/div`).boundingBox();
        const compareImage = await this._page.locator(`//div[@class='typeName' and text()='${iconName}']/../preceding-sibling::div`);

        let sourceImageScreenshot;
        let compareImageScreenshot;

        if (sourceImage) {
            sourceImageScreenshot = await this._page.screenshot({
                clip: { x: sourceImage.x, y: sourceImage.y, width: sourceImage.width, height: sourceImage.height }
            });
        }

        await compareImage.scrollIntoViewIfNeeded();
        const compareImageElement = await compareImage.boundingBox();

        if (compareImageElement) {
            compareImageScreenshot = await this._page.screenshot({
                clip: { x: compareImageElement.x, y: compareImageElement.y, width: compareImageElement.width, height: compareImageElement.height }
            });
        }

        const mismatchedPixels = await ImageCompare.compareImages(sourceImageScreenshot, compareImageScreenshot, iconName);

        return mismatchedPixels;
    }

    public async deleteEmdeddedXyicon() {
        const responePromise = this._page.waitForResponse(response =>
            response.url() === `${this._apiEndPoint}/xyicons/delete` && response.status() === 200
        );
        await this._page.keyboard.press('Delete');
        await this.btnConfirmDelete.click();
        await responePromise;
    }
}