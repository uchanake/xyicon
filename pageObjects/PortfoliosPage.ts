import { Locator, Page } from "playwright";

export default class PortfoliosPage {

    private _page: Page;

    private cellPortfolios: Locator;

    constructor(page: Page) {
        this._page = page;
        this.cellPortfolios = this._page.locator("#body div");
    }

    public async selectPortfolioByName(portfolioName: string) {
        const cellPortfolio = await this.cellPortfolios.filter({ hasText: portfolioName }).nth(3);
        await cellPortfolio.dblclick();
    }
}