import { Locator, Page } from 'playwright';

export default class SpacePage {
    private _page: Page;
    private thumbSpace: Locator;

    constructor(page: Page) {
        this._page = page;
        this.thumbSpace = this._page.locator('.thumbnail');
    }

    public async selectSpace(spaceIndex: number = 0) {
        await this.thumbSpace.nth(spaceIndex).dblclick();
    }
}
