import { Locator, Page } from "@playwright/test";

export default class LoginPage {

    private _page: Page;

    private readonly txtUsername: Locator;
    private readonly txtPassword: Locator;
    private readonly btnLogin: Locator;

    constructor(page: Page) {
        this._page = page;
        this.txtUsername = page.locator('#userName');
        this.txtPassword = page.locator('#pwInput');
        this.btnLogin = page.locator('//button[@name="submitButton"]');
    }

    public async loadBaseUrl() {
        await this._page.goto('/');
    }

    public async loginAs(username: string, password: string) {
        await this._page.goto('/');
        await this.txtUsername.fill(username);
        await this.txtPassword.fill(password);
        await this.btnLogin.click();
    }
}