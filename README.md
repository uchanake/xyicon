
# Xyicon Automation Assignment

## Description

This automation Project is implemented using following tools and technologies
1. TypeScript
2. Playwright
3. pixelmatch for image compare
4. allure-playwright as reporting tool
5. GitLab as repository

## Automation Scope
1. API Endpoint : https://api.xyicon.com/
2. Web URL : https://app.xyicon.com/


## Test Execution Instructions (Running tests in locally)
**Pre-condition**: Node 20+

Follow the steps below to execute tests
1. Clone the project from https://gitlab.com/uchanake/xyicon
2. Open project location with VSCODE
3. Enter in vscode terminal `npm install`
4. Enter in vscode terminal `npx playwright install --with-deps`
5. To run the tests
    1. Tests runs all projects: `npm run test:all`
    2. Test runs on chromium browser: `npm run test:chrome`
    3. Test runs on firefox browser: `npm run test:firefox`
    4. Test runs on edge browser: `npm run test:edge`
    5. Show report after test execution: `npm run show-allure-report`
    6. Show default playwright report: `npm run show-report`

   Wait for the  ✨Magic ✨

Tests will run on Chrome, Firefox and Edge browsers, with one worker(parallel execution in a squention way as the same login action can be effected to the other instance).

## Test cases
**Web ui tests**
1. validate embedded mouse icon with computer is matching with the catalog mouse thumbnail.

## Running in GitLab - CI integration
CI build will start with each merge to the main automatically, which will execute the automated test cases. Also the pipeline can be triggered manually.

