import test, { expect } from '../customFixtures/CustomFixture';
import deviceList from '../tests/data/deviceList.json';
import loginData from '../tests/data/loginData.json';
import plotData from '../tests/data/plotData.json';

test.describe('add device to level 1 canva tests', () => {

    test.beforeEach(async ({ loginPage, portfoliosPage, spacePage }) => {
        await loginPage.loginAs(loginData.adminuser.username, loginData.adminuser.password);
        await portfoliosPage.selectPortfolioByName(plotData.plotDetails[0].plotName);
        await spacePage.selectSpace();
    });

    test("validate add mouse device to computer", async ({ spaceLevel1Page }) => {
        await spaceLevel1Page.ZoomOutSpace();
        await spaceLevel1Page.lockCatalogWindow();
        await spaceLevel1Page.showHideRightWindow();
        await spaceLevel1Page.dragDropDevice(deviceList.devices.computer);
        await spaceLevel1Page.dragDropDevice(deviceList.devices.mouse);
        await spaceLevel1Page.selectObejctForDetails();
        await spaceLevel1Page.showHideRightWindow();

        const mismatchedPixels = await spaceLevel1Page.compareEmbeddedXyicon(deviceList.devices.mouse);

        expect(mismatchedPixels).toEqual(0);
    });

    test.afterEach(async ({ spaceLevel1Page }) => {
        await spaceLevel1Page.showHideRightWindow();
        await spaceLevel1Page.deleteEmdeddedXyicon();
    });
});