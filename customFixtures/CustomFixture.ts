import { test as base } from '@playwright/test';

import LoginPage from '../pageObjects/LoginPage';
import PortfoliosPage from '../pageObjects/PortfoliosPage';
import SpaceLevel1Page from '../pageObjects/SpaceLevel1Page';
import SpacePage from '../pageObjects/SpacePage';

type Pages = {
    loginPage: LoginPage,
    portfoliosPage: PortfoliosPage,
    spacePage: SpacePage,
    spaceLevel1Page: SpaceLevel1Page
}


const test = base.extend<Pages>({
    loginPage: async ({ page }, use) => {
        const logingPage = new LoginPage(page);
        await use(logingPage);
    },
    portfoliosPage: async ({ page }, use) => {
        const portfoliosPage = new PortfoliosPage(page);
        await use(portfoliosPage);
    },
    spacePage: async ({ page }, use) => {
        const spacePage = new SpacePage(page);
        await use(spacePage);
    },
    spaceLevel1Page: async ({ page, baseURL, browserName }, use) => {
        const spaceLevel1Page = new SpaceLevel1Page(page, baseURL!, browserName);
        await use(spaceLevel1Page);
    }
});

export default test;
export const expect = test.expect;