const fs = require('fs');
const { PNG } = require('pngjs');
const pixelmatch = require('pixelmatch');

export default class ImageCompare {

    public static async compareImages(sourceImageScreenshot: Buffer, compareImageScreenshot: Buffer, diffImgName: string): Promise<number> {
        const sourceImageScreenshotPng = await PNG.sync.read(sourceImageScreenshot);
        const compareImageScreenshotPng = await PNG.sync.read(compareImageScreenshot);

        const { width, height } = sourceImageScreenshotPng;
        const imageDiff = new PNG({ width, height });

        const mismatchedPixels = await pixelmatch(sourceImageScreenshotPng.data, compareImageScreenshotPng.data, 
            imageDiff.data, width, height, { threshold: 0.2 });
        const diffFileName = `./test-image-results/${diffImgName}.png`;
        await fs.writeFileSync(diffFileName, PNG.sync.write(imageDiff));

        return mismatchedPixels;
    }
}